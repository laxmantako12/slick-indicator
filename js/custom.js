$(document).ready(function () {
    var aboutSlider = $('.slider-for');
    $('.slider-for').slick({
        // dots: true
        infinite: true,
        arrows: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        mobileFirst: true,
        autoplay: false,
        autoplaySpeed: 5000,
         asNavFor: '.nav-for'
        // prevArrow: "<img src='https://image.ibb.co/h2WX0a/arrow_left.png'>",
        // nextArrow: "<img src='../img/background/arrow-right.png'>"
    });

     $('.nav-for').slick({
         // dots: true
         infinite: true,
         arrows: false,
         slidesToShow: 4,
         slidesToScroll: 1,
          asNavFor: '.slider-for',
         mobileFirst: true,
         autoplay: false,
         autoplaySpeed: 5000
         // prevArrow: "<img src='https://image.ibb.co/h2WX0a/arrow_left.png'>",
         // nextArrow: "<img src='../img/background/arrow-right.png'>"
     });

    $(".arrow-next").on("click", function () {
        $('.slider-for').slick("slickNext");
        //$(".individual-indicators .slide").removeClass("about-slider-active");
        var currentSlideIndex = $(".slider-for").slick("slickCurrentSlide")
    });
    $(".arrow-prev").on("click", function () {
        $(".slider-for").slick("slickPrev");
        //$(".individual-indicators .slide").removeClass("about-slider-active");
        var currentSlideIndex = $(".slider-for").slick("slickCurrentSlide");
    });

    $('.slider').on('swipe', function (event, slick, direction) {
        // var current = $(this).attr('data-slider');
        if (direction == 'left') {
            $('.slider-for').slick("slickNext");
        } else if (direction == 'right') {
            $(".slider-for").slick("slickPrev");
        }

    });

    $('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var left_val, li_wid = parseInt($(".individual-indicators .slide").css("width"));
        var currentSlideIndex = nextSlide;
        //$(".individual-indicators .slide").removeClass("about-slider-active");
        //var currentSlideIndex = $("li.slick-acitve").attr('id');
        left_val = currentSlideIndex * li_wid + 0;
        $(".about-active-indicator").css({
            left: left_val
        });
    });
    var indicatorWidht = $('.nav-for .slick-slide').innerWidth();
    $('.about-active-indicator').css({
        'width': indicatorWidht,
    });
})


